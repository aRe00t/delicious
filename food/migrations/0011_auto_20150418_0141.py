# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0010_auto_20150418_0134'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='supplier',
            name='tweet',
        ),
        migrations.AddField(
            model_name='supplier',
            name='tweet',
            field=models.ManyToManyField(to='food.Tweet'),
        ),
    ]
