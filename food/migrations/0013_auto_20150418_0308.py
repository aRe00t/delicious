# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0012_auto_20150418_0231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='price',
            field=models.IntegerField(default=0),
        ),
    ]
