# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0008_auto_20150418_0129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='procedure',
            name='content',
            field=models.CharField(max_length=500),
        ),
    ]
