# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0016_order_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='has_paid',
            field=models.BooleanField(default=False),
        ),
    ]
