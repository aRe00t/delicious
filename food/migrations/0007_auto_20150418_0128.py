# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0006_auto_20150418_0128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='procedure',
            name='content',
            field=models.TextField(max_length=250, blank=True),
        ),
        migrations.AlterField(
            model_name='procedure',
            name='picture',
            field=models.CharField(max_length=200),
        ),
    ]
