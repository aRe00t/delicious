# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0011_auto_20150418_0141'),
    ]

    operations = [
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.FloatField(default=0)),
            ],
        ),
        migrations.RemoveField(
            model_name='supplier',
            name='tweet',
        ),
        migrations.AddField(
            model_name='price',
            name='supplier',
            field=models.ForeignKey(to='food.Supplier'),
        ),
        migrations.AddField(
            model_name='price',
            name='tweet',
            field=models.ForeignKey(to='food.Tweet'),
        ),
    ]
