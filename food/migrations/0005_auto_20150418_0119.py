# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0004_auto_20150417_1444'),
    ]

    operations = [
        migrations.AlterField(
            model_name='procedure',
            name='picture',
            field=models.CharField(max_length=200, blank=True),
        ),
    ]
